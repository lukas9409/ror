class Spot < ActiveRecord::Base
	has_many :reservations

	def self.search(search)
	  if search
	    find(:all, :conditions => ['name LIKE ?', "%#{search}%"])
  	end
	end
end
