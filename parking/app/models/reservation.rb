class Reservation < ActiveRecord::Base
	belongs_to :spot 
	belongs_to :reservation_time
	belongs_to :user
	validates :time_range, presence:true

	def is_not_available(start_epoch, end_epoch)
		requested_reservation_start_epoch = requested_datetime.to_time.to_i
		requested_reservation_end_epoch = requested_datetime.to_time.to_i + (3600 * time_range.to_i)

		return requested_reservation_start_epoch.between?(start_epoch + 1, end_epoch - 1) || requested_reservation_end_epoch.between?(start_epoch + 1, end_epoch - 1) || 
				( start_epoch >= requested_reservation_start_epoch && end_epoch <= requested_reservation_end_epoch)
	end
end
