class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = Reservation.all
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
    @reservation = Reservation.new(reservation_params)
    reserved_spot = Spot.find(@reservation.spot_id)

    if !logged_in?
      flash[:danger] = "You need to log in!"
      redirect_to reserved_spot
      return
    end 

    reserved_spot.reservations.each do |spot_reservation|
      start_epoch = spot_reservation.requested_datetime.to_time.to_i
      end_epoch = spot_reservation.requested_datetime.to_time.to_i + (3600 * spot_reservation.time_range.to_i)

      if @reservation.is_not_available(start_epoch, end_epoch)
        flash[:danger] = 'Reservation cannot be made. This date have been reserved.'
        redirect_to reserved_spot
        return
      end   
    end

    if @reservation.save
      flash[:success] = 'Reservation was successfully created.'
      redirect_to reserved_spot
    else
      flash[:danger] =  @reservation.errors.full_messages
      redirect_to reserved_spot
    end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    reserved_spot = Spot.find(@reservation.spot_id)
    flash[:success] = 'Reservation was successfully deleted.'
    redirect_to reserved_spot
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(:spot_id, :requested_datetime, :time_range, :reservation_time_id, :user_id)
    end
end
