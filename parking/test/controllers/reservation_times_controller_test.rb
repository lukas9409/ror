require 'test_helper'

class ReservationTimesControllerTest < ActionController::TestCase
  setup do
    @reservation_time = reservation_times(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reservation_times)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reservation_time" do
    assert_difference('ReservationTime.count') do
      post :create, reservation_time: { duration: @reservation_time.duration, start_datetime: @reservation_time.start_datetime }
    end

    assert_redirected_to reservation_time_path(assigns(:reservation_time))
  end

  test "should show reservation_time" do
    get :show, id: @reservation_time
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reservation_time
    assert_response :success
  end

  test "should update reservation_time" do
    patch :update, id: @reservation_time, reservation_time: { duration: @reservation_time.duration, start_datetime: @reservation_time.start_datetime }
    assert_redirected_to reservation_time_path(assigns(:reservation_time))
  end

  test "should destroy reservation_time" do
    assert_difference('ReservationTime.count', -1) do
      delete :destroy, id: @reservation_time
    end

    assert_redirected_to reservation_times_path
  end
end
