class AddTimeRangeToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :time_range, :integer
  end
end
