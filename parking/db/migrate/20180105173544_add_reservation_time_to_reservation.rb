class AddReservationTimeToReservation < ActiveRecord::Migration
  def change
    add_reference :reservations, :reservation_time, index: true, foreign_key: true
  end
end
