class CreateReservationTimes < ActiveRecord::Migration
  def change
    create_table :reservation_times do |t|
      t.datetime :start_datetime
      t.integer :duration

      t.timestamps null: false
    end
  end
end
