class RemoveReservationFromReservationTime < ActiveRecord::Migration
  def change
    remove_reference :reservation_times, :reservation, index: true, foreign_key: true
  end
end
