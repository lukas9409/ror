class AddSpotToReservations < ActiveRecord::Migration
  def change
    add_reference :reservations, :spot, index: true, foreign_key: true
  end
end
