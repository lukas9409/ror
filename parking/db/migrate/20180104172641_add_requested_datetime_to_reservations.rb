class AddRequestedDatetimeToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :requested_datetime, :datetime
  end
end
