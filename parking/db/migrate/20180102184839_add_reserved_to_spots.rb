class AddReservedToSpots < ActiveRecord::Migration
  def change
    add_column :spots, :reserved, :string
  end
end
