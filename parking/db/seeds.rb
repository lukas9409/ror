# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Spot.destroy_all

for i in (0...8)
	for j in (0...20)
		Spot.create("reserved"=>"no", "x"=>j, "y"=>i)
	end
end

Reservation.destroy_all

spot = Spot.first
spot.save

User.destroy_all

User.create!(name:  "Krystian",
						 surname:  "Zak",
						 email: "krystian@rail.org",
						 password:              "foobar",
						 password_confirmation: "foobar",
						 admin: true)
user = User.first
user.save						 

Reservation.create(
	requested_datetime: '2018-01-01 14:00:00',
	time_range: 2,
	spot_id: spot.id,
	user_id: user.id)

99.times do |n|
	name  = Faker::Name.name
	surname  = Faker::Name.last_name
	email = "example-#{n+1}@railstutorial.org"
	password = "password"
	User.create!(name:  name,
							 surname:  surname,
							 email: email,
							 password:              password,
							 password_confirmation: password)
end